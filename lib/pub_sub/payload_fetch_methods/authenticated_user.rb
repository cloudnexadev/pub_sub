module PubSub
  module PayloadFetchMethods
    class AuthenticatedUser < GlobalsFetcher

      def initialize(options = {})
        method_name = options[:current_user_method] || :current_user
        super(:controller, method_name)
      end

    end
  end
end