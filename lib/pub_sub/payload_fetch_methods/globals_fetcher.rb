module PubSub
  module PayloadFetchMethods
    class GlobalsFetcher

      def initialize(globals_key, method_name, default = nil)
        @globals_key = globals_key
        @method_name = method_name
        @default = default
      end

      def fetch_data(event, publisher)
        g = PubSub::GlobalStore.get(@globals_key)
        g.respond_to?(@method_name) ? g.send(@method_name) : @default
      end

    end
  end
end