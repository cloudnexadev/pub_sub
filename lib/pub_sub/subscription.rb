module PubSub
  class Subscription

    DEFAULT_PRIORITY = 0

    attr_reader :on, :subscriber, :with, :prefix, :allowed_classes, :_priority

    def initialize(subscriber, options)
      @subscriber = subscriber
      @on = Array(options.fetch(:on) { 'all' }).map(&:to_s)
      @with   = options[:with]
      @prefix = stringify_prefix(options[:prefix])
      @allowed_classes = Array(options[:scope]).map(&:to_s).to_set
      @_priority = options[:priority] || DEFAULT_PRIORITY
    end

    def publish(event, publisher, payload = {})
      if event_in_scope?(event) && publisher_in_scope?(publisher)

        if subscriber.is_a?(Proc) # block

          subscriber.call(event, publisher, payload)

        else # object

          in_subscriber_scope = true
          in_subscriber_scope = subscriber.broadcast_in_scope?(event, publisher, payload) if subscriber.respond_to?(:broadcast_in_scope?)

          if in_subscriber_scope
            method_to_call = map_event_to_method(event)
            if subscriber.respond_to?(method_to_call)
              subscriber.public_send(method_to_call, event, publisher, payload)
            end
          end

        end

      end
    end

    def priority(event, publisher, payload = {})
      if subscriber.respond_to?(:priority)
        subscriber.priority(event, publisher, payload)
      else
        _priority
      end
    end


    private

    def event_in_scope?(event)
      on.include?(event) || on.include?('all')
    end

    def publisher_in_scope?(publisher)
      allowed_classes.empty? || publisher.class.ancestors.any? { |ancestor| allowed_classes.include?(ancestor.to_s) }
    end

    def map_event_to_method(event)
      prefix + (with || event).to_s
    end

    def stringify_prefix(_prefix)
      case _prefix
        when nil
          ''
        when true
          default_prefix + '_'
        else
          _prefix.to_s + '_'
      end
    end

    def default_prefix
      'on'
    end


  end
end