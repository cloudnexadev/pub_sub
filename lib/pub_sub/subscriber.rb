module PubSub
  module Subscriber
    extend ActiveSupport::Concern

    m = Module.new do
      module_eval do
        def subscribe_to(publisher, options = {})
          PubSub.add_subscription(self, options) and return if publisher == :all

          is_a_const = publisher.class == Class
          if is_a_const
            PubSub.add_subscription(self, options.merge({scope: publisher}))
          else
            raise "publisher doesn't allow subscriptions, check you included PubSub::Publisher." unless publisher.respond_to?(:subscribe_with)
            publisher.subscribe_with(self, options.merge({scope: publisher}))
          end
        end
      end
    end

    included do
      include m
      extend m
    end

    module ClassMethods
    end

  end
end