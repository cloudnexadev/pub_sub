module PubSub
  module SubscriptionProviders
    class InMemoryProvider < Base

      def initialize
        super
        @store = Common::InMemorySetStore.new
      end

      def subscriptions
        @store.to_a
      end

      def find(event, publisher, payload)
        super
      end

      def add(subscriber, options = {})
        @store.add(Subscription.new(subscriber, options))
        self
      end

      def add_block(options, block)
        @store.add(Subscription.new(block, options))
        self
      end

      def clear
        @store.clear
      end

    end
  end
end