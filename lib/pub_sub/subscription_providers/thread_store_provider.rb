module PubSub
  module SubscriptionProviders
    class ThreadStoreProvider < Base

      def initialize
        super
      end

      def subscriptions
        store.to_a
      end

      def find(event, publisher, payload)
        super
      end

      def add(subscriber, options = {})
        store.add(Subscription.new(subscriber, options))
        self
      end

      def add_block(options, block)
        store.add(Subscription.new(block, options))
        self
      end

      def clear
        store.clear
      end

      def with(subscribers, options, &block)
        begin
          subscribers.each { |subscriber| add(subscriber, options)}
          yield
        ensure
          clear
        end
      end

      private

      def store
        Thread.current[key] ||= Set.new
      end

      def key
        '__pub_sub_temporary_subscriptions'
      end

      class << self

        def with(*subscribers, &block)
          options = subscribers.last.is_a?(Hash) ? subscribers.pop : {}
          new.with(subscribers, options, &block)
        end

        def subscriptions
          new.subscriptions
        end

      end

    end
  end
end