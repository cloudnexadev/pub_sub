module PubSub
  module SubscriptionProviders
    class Base

      def subscriptions
        raise NotImplementedError
      end

      def subscribers
        subscriptions.map(&:subscriber).freeze
      end

      def find(event, publisher, payload)
        raise NotImplementedError
      end

      def add(subscriber, options = {})
        raise NotImplementedError
      end

      def add_block(options = {}, &block)
        raise NotImplementedError
      end

      def clear
        raise NotImplementedError
      end

    end
  end
end