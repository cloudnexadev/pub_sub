require 'active_support/concern'

# http://coderrr.wordpress.com/2008/04/10/lets-stop-polluting-the-threadcurrent-hash/
module PubSub
  module Common
    module ThreadLocalAccessor

      def thread_local_accessor(*args)
        options = args.last.is_a?(Hash) ? args.pop : {}

        args.each do |attr|
          raise TypeError.new('attribute name is not symbol') unless attr.is_a?(Symbol)

          ivar = "@_tla_#{attr}"

          instance_variable_set(ivar, Hash.new {|h,k| h[k] = options[:default]})

          define_singleton_method(attr) do
            instance_variable_get(ivar)[Thread.current.object_id]
          end

          define_singleton_method("#{attr}=") do |val|
            thread_id = Thread.current.object_id
            unless instance_variable_get(ivar).has_key?(thread_id)
              ObjectSpace.define_finalizer(Thread.current, lambda { instance_variable_get(ivar).delete(thread_id) })
            end
            instance_variable_get(ivar)[thread_id] = val
          end

          define_singleton_method("#{attr}_reset_on_all_threads") do
            instance_variable_get(ivar).clear
          end

          # if [:private,:protected].include?(options[:modifier])
          #   self.class.send(options[:modifier], attr)
          #   self.class.send(options[:modifier], "#{attr}=")
          # end
        end

        # instance_eval do
        #   instance_variable_set "@#{name}_default", (options[:default].dup rescue options[:default])
        #   instance_variable_set "@#{name}", Hash.new {|h,k| h[k] = options[:default]}
        # end
        # instance_eval %{
        #   #{options[:modifier].to_s if [:private,:protected].include?(options[:modifier])}
        #   FINALIZER = lambda {|id| @#{name}.delete id }
        #
        #   def #{name}
        #     puts "get: ""@#{name}.inspect"
        #     @#{name}[Thread.current.object_id]
        #   end
        #
        #   def #{name}=(val)
        #     ObjectSpace.define_finalizer(Thread.current, FINALIZER) unless @#{name}.has_key?(Thread.current.object_id)
        #     @#{name}[Thread.current.object_id] = val
        #   end
        #
        #   def #{name}_reset_on_all_threads
        #     @#{name} = {} # @#{name}_default
        #   end
        #   }
      end


    end
  end

end