require 'set'

module PubSub
  module Common
    class InMemorySetStore < Set

      attr_reader :mutex
      private :mutex

      def initialize
        super
        @mutex = Mutex.new
      end

      def add(o)
        with_mutex { super(o) }
      end

      def delete(o)
        with_mutex { super(o) }
      end

      def clear
        with_mutex { super }
      end

      private

      def with_mutex
        mutex.synchronize { yield }
      end

    end
  end
end