require 'singleton'

module PubSub
  class Singleton
    include ::Singleton

    attr_reader :mutex
    private :mutex

    def initialize
      @mutex         = Mutex.new
    end

    protected

    def with_mutex
      mutex.synchronize { yield }
    end

  end
end