module PubSub
  module Common
    class InMemoryKeyValueStore < Hash

      attr_reader :mutex
      private :mutex

      def initialize
        super
        @mutex = Mutex.new
      end

      def []=(key, value)
        with_mutex { super(key, value) }
      end

      def delete(key)
        with_mutex { super(key) }
      end

      def clear
        with_mutex { super }
      end

      private

      def with_mutex
        mutex.synchronize { yield }
      end

    end
  end
end