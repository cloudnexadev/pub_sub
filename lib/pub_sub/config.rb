module PubSub
  # Class used to initialize configuration object.
  class Config
    attr_accessor :enabled, :global_payload, :default_subscription_provider
    attr_reader :custom_subscription_providers

    def initialize
      reset
    end

    def enabled?
      enabled
    end

    def reset
      @enabled = true
      @global_payload = {}
      if @default_subscription_provider
        @default_subscription_provider.clear
      else
        @default_subscription_provider = SubscriptionProviders::InMemoryProvider.new
      end
      @custom_subscription_providers = []
    end
  end
end