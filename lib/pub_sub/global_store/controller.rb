module PubSub
  module GlobalStore
    module Controller
      extend ActiveSupport::Concern

      included do
        around_filter :store_controller_in_globals
      end

      private

      # Before filter executed to remember current controller
      def store_controller_in_globals
        PubSub::GlobalStore.set(:controller, self)
        yield
        PubSub::GlobalStore.remove(:controller)
      end
    end
  end
end

# if defined?(ActionController) and defined?(ActionController::Base)
#   ActionController::Base.include PubSub::GlobalStore::Controller
# end