require 'active_support/concern'

module PubSub
  module Publisher
    extend ActiveSupport::Concern

    included do

      def subscribers
        subscriptions.map(&:subscriber).freeze
      end

      def subscribe_with(*args, &block)
        raise 'no class/instance or block subscribers.' if args.size == 0 && !block_given?

        options = args.last.is_a?(Hash) ? args.pop : {}
        local_provider.add_block(options, block) if block_given?
        args.each do |subscriber|
          local_provider.add(subscriber, options)
        end

        self
      end

      # sugar
      def respond_to(*events, &block)
        subscribe_with({:on => events}, &block)
      end
      alias :on :respond_to

      def publish(event, payload = {})
        return unless PubSub.enabled?

        ordered_subscriptions = subscriptions.sort_by!{|r| r.priority(event, self, payload)}

        cleaned_event =  clean_event(event)
        actual_payload = global_payload(cleaned_event, payload.keys).merge(payload)

        ordered_subscriptions.each do | subscription |
          subscription.publish(cleaned_event, self, actual_payload)
        end

        nil
      end
      alias :announce :publish

      private

      # evaluating all global payload keys except
      def global_payload(event, except_keys = [])
        payload = {}
        keys_to_evaluate = PubSub.config.global_payload.keys - except_keys
        keys_to_evaluate.each do |key|
          value_expression = PubSub.config.global_payload[key]
          # const value
          value = value_expression
          # proc
          value = value_expression.call(event, self) if value_expression.is_a? Proc
          # fetch strategy
          value = value_expression.fetch_data(event, self) if value_expression.respond_to?(:fetch_data)
          payload[key] = value
        end
        payload
      end

      def local_provider
        @local_provider ||= SubscriptionProviders::InMemoryProvider.new
      end

      def local_subscriptions
        local_provider.subscriptions
      end

      def temporary_subscriptions
        SubscriptionProviders::ThreadStoreProvider.subscriptions
      end

      def global_subscriptions
        PubSub.subscriptions
      end

      def subscriptions
        local_subscriptions + global_subscriptions + temporary_subscriptions
      end

      def clean_event(event)
        event.to_s.gsub('-', '_')
      end

    end

    module ClassMethods

      def subscribe_with(*args, &block)
        raise 'no class/instance or block subscribers.' if args.size == 0 && !block_given?

        options = args.last.is_a?(Hash) ? args.pop : {}
        options.merge(:scope => self)

        PubSub.config.default_subscription_provider.add_block(options, block) if block_given?
        args.each do |subscriber|
          PubSub.config.default_subscription_provider.add(subscriber, options)
        end
      end

    end

  end
end
