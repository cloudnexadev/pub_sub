module PubSub
  module Deactivatable
    extend ActiveSupport::Concern

    module ClassMethods

      # Switches PubSub on or off.
      # @param value [Boolean]
      def enabled=(value)
        PubSub.config.enabled = value
      end

      # Returns `true` if PubSub is on, `false` otherwise.
      # Enabled by default.
      # @return [Boolean]
      def enabled?
        !!PubSub.config.enabled
      end

    end

  end
end