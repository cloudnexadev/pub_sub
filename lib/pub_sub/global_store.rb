module PubSub
  module GlobalStore
    extend Common::ThreadLocalAccessor

    thread_local_accessor :globals, {:default => {}, :modifier => :private}

    def self.set(key, value)
      new_globals = self.send(:globals).dup
      new_globals[key] = value
      self.send('globals=', new_globals)
    end

    def self.get(key)
      self.send(:globals)[key]
    end

    def self.keys
      self.send(:globals).keys
    end

    def self.remove(key)
      self.send(:globals).delete(key)
    end

    def self.reset
      self.send(:globals_reset_on_all_threads)
    end

  end
end