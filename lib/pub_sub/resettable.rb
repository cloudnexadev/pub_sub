module PubSub
  module Resettable
    extend ActiveSupport::Concern

    included do
    end

    module ClassMethods

      def reset
        PubSub.config.reset
        GlobalStore.reset
      end

    end

  end
end