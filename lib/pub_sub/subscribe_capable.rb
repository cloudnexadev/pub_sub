module PubSub
  module SubscribeCapable
    extend ActiveSupport::Concern

    included do
    end

    module ClassMethods

      def subscription_providers
        [PubSub.config.default_subscription_provider] |  PubSub.config.custom_subscription_providers
      end

      def subscriptions
        subscription_providers.flat_map(&:subscriptions)
      end

      def add_subscriptions(*args, &block)
        raise 'no class/instance or block subscribers.' if args.size == 0 && !block_given?

        options = args.last.is_a?(Hash) ? args.pop : {}
        PubSub.config.default_subscription_provider.add_block(options, block) if block_given?
        args.each do |subscriber|
          PubSub.config.default_subscription_provider.add(subscriber, options)
        end
      end
      alias :add_subscription :add_subscriptions

      def using_subscribers(*args, &block)
        SubscriptionProviders::ThreadStoreProvider.with(*args, &block)
      end

    end

  end
end