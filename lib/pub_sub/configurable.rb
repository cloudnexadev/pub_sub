module PubSub
  # Class used to initialize configuration object.
  module Configurable
    extend ActiveSupport::Concern

    module ClassMethods

      def config
        @config ||= PubSub::Config.new
      end

      def configure(&block)
        yield(config) if block_given?
      end
    end

  end
end