require 'spec_helper'

describe PubSub do

  describe 'Configurable' do

    it '.configure allows fluent configuration' do
      PubSub.configure do |config|
        config.enabled = false
        config.global_payload = {:a => 1}
      end

      expect(PubSub.config.enabled).to be_falsey
      expect(PubSub.config.global_payload).to eq({:a => 1})
    end

  end

  describe 'SubscribeCapable' do

    it 'throws error when no class/instance/block subscribers provided' do
      expect { PubSub.add_subscription }.to raise_error('no class/instance or block subscribers.')
    end

    it 'allows adding block subscription with default options' do
      PubSub.add_subscription{}
      expect(PubSub.subscriptions.size).to eq(1)
      expect(PubSub.subscriptions.first.subscriber).to be_an_instance_of(Proc)
    end

    it 'allows adding block subscription with custom options' do
      PubSub.add_subscription({on: :test}){}
      expect(PubSub.subscriptions.size).to eq(1)
      expect(PubSub.subscriptions.first.subscriber).to be_an_instance_of(Proc)
    end

    it 'allows adding custom subscriber with default options' do
      PubSub.add_subscription(double('subscriber'))
      expect(PubSub.subscriptions.size).to eq(1)
    end

    it '.using_subscribers subscribes subscribers to all broadcast events for the duration of block' do
      publisher = publisher_class.new
      subscriber = double('subscriber')

      expect(subscriber).to receive(:im_here)
      expect(subscriber).not_to receive(:not_here)

      PubSub.using_subscribers(subscriber) do
        publisher.send(:publish, 'im_here')
      end

      publisher.send(:publish, 'not_here')
    end

  end

  describe 'Resettable' do

    it '.reset clears all custom providers and subscribers from default provider' do
      PubSub.configure do |config|
        config.custom_subscription_providers << double('provider')
      end
      PubSub.add_subscription(double('subscriber'))
      PubSub.reset
      expect(PubSub.config.custom_subscription_providers.size).to eq(0)
      expect(PubSub.config.default_subscription_provider.subscriptions.size).to eq(0)
    end

  end

end