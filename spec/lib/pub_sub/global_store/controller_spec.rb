require 'spec_helper'

class StoringController < ActionController::Base
  include PubSub::GlobalStore::Controller
end

describe PubSub::GlobalStore::Controller do

  it 'stores controller with a around filter in controller' do
    controller = StoringController.new
    expect(controller._process_action_callbacks.select {|c| c.kind == :around}.map(&:filter)).to include(:store_controller_in_globals)
    # controller.instance_eval { store_controller_in_globals }
    # expect(PubSub::GlobalStore.get(:controller)).to be(controller)
  end

  it 'stores controller in a threadsafe way' do
    controller = double('controller')
    PubSub::GlobalStore.set(:controller, controller)
    expect(PubSub::GlobalStore.get(:controller)).to be(controller)

    with_new_thread do
      another_controller = double('another_controller')
      PubSub::GlobalStore.set(:controller, another_controller)
      expect(PubSub::GlobalStore.get(:controller)).to be(another_controller)
    end

    expect(PubSub::GlobalStore.get(:controller)).to be(controller)
  end

end