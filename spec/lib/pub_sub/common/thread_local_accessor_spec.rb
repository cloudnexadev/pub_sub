require 'no_pub_sub_spec_helper'
require 'pub_sub/common/thread_local_accessor'
require 'support/threading'

describe PubSub::Common::ThreadLocalAccessor do

  let!(:class_with_public_tla) do
    Class.new do
      extend PubSub::Common::ThreadLocalAccessor
      thread_local_accessor :some_setting, :default => :default
    end
  end

  it 'sets accessor default value for the current thread' do
    expect(class_with_public_tla.some_setting).to eq(:default)
  end

  it 'sets accessor default value in a new thread' do
    with_new_thread { expect(class_with_public_tla.some_setting).to eq(:default) }
  end

  it "doesn't affect other threads values" do
    class_with_public_tla.some_setting = 5
    with_new_thread do
      expect(class_with_public_tla.some_setting).to eq(:default)
      class_with_public_tla.some_setting = 10
    end
    expect(class_with_public_tla.some_setting).to eq(5)
  end

  it 'stores value using class in the current thread' do
    class_with_public_tla.some_setting = 5
    expect(class_with_public_tla.some_setting).to eq(5)
  end

  it 'stores value using class in a new thread' do
    with_new_thread do
      class_with_public_tla.some_setting = 5
      expect(class_with_public_tla.some_setting).to eq(5)
    end
  end

  it '.reset_on_all_threads sets attribute to default value' do
    class_with_public_tla.some_setting = 5
    with_new_thread { class_with_public_tla.some_setting = 10 }

    class_with_public_tla.some_setting_reset_on_all_threads
    expect(class_with_public_tla.some_setting).to eq(:default)
  end

  # context 'with private modifier' do
  #
  #   let!(:class_with_private_tla) do
  #     Class.new do
  #       extend PubSub::Common::ThreadLocalAccessor
  #       thread_local_accessor :some_setting, :default => :default, :modifier => :private
  #     end
  #   end
  #
  #   it 'should not respond to getter and setter' do
  #     expect(class_with_private_tla).to_not respond_to(:some_setting, 'some_setting=')
  #   end
  #
  # end

end