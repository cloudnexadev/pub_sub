require 'spec_helper'

describe PubSub::PayloadFetchMethods::AuthenticatedUser do

  let(:controller) { double('controller') }
  let(:publisher) { double('publisher') }

  it 'returns nil if no controller in globals' do
    expect(subject.fetch_data(:event, publisher)).to be_nil
  end

  it "returns nil if controller doesn't respond to current user method" do
    PubSub::GlobalStore.set(:controller, controller)
    expect(subject.fetch_data(:event, publisher)).to be_nil
  end

  it 'calls current user method on controller to get user' do
    expect(controller).to receive(:current_user).and_return(:user)
    PubSub::GlobalStore.set(:controller, controller)
    expect(subject.fetch_data(:event, publisher)).to eq(:user)
  end

  it 'custom current user method can be used' do
    method = described_class.new({:current_user_method => :app_user})
    expect(controller).to receive(:app_user).and_return(:user)
    expect(controller).not_to receive(:current_user)

    PubSub::GlobalStore.set(:controller, controller)
    expect(method.fetch_data(:event, publisher)).to eq(:user)
  end

end