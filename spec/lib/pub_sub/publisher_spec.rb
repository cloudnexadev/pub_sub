require 'spec_helper'

describe PubSub::Publisher do
  let(:subscriber)  { double('subscriber') }
  let(:publisher) { publisher_class.new }

  describe '.subscribe_with' do
    it 'subscribes given subscriber to all published events' do
      expect(subscriber).to receive(:this_happened)
      expect(subscriber).to receive(:so_did_this)

      publisher.subscribe_with(subscriber)

      publisher.send(:publish, :this_happened)
      publisher.send(:publish, :so_did_this)
    end

    describe ':on argument' do
      it 'subscribes given subscriber to a single event' do
        expect(subscriber).to receive(:this_happened)
        allow(subscriber).to receive(:so_did_this)
        expect(subscriber).not_to receive(:so_did_this)

        expect(subscriber).to respond_to(:so_did_this)

        publisher.subscribe_with(subscriber, :on => :this_happened)

        publisher.send(:publish, :this_happened)
        publisher.send(:publish, :so_did_this)
      end

      it 'subscribes given subscriber to many events' do
        expect(subscriber).to receive(:this_happened)
        expect(subscriber).to receive(:and_this)
        allow(subscriber).to receive(:so_did_this)
        expect(subscriber).not_to receive(:so_did_this)

        expect(subscriber).to  respond_to(:so_did_this)

        publisher.subscribe_with(subscriber, :on => [:this_happened, :and_this])

        publisher.send(:publish, :this_happened)
        publisher.send(:publish, :so_did_this)
        publisher.send(:publish, :and_this)
      end
    end

    describe ':with argument' do
      it 'sets method to call subscriber with on event' do
        expect(subscriber).to receive(:different_method).twice

        publisher.subscribe_with(subscriber, :with => :different_method)

        publisher.send(:publish, :this_happened)
        publisher.send(:publish, :so_did_this)
      end
    end

    describe ':prefix argument' do
      it 'prefixes broadcast events with given symbol' do
        expect(subscriber).to receive(:after_it_happened)
        expect(subscriber).not_to receive(:it_happened)

        publisher.subscribe_with(subscriber, :prefix => :after)

        publisher.send(:publish, :it_happened)
      end

      it 'prefixes broadcast events with "on" when given true' do
        expect(subscriber).to receive(:on_it_happened)
        expect(subscriber).not_to receive(:it_happened)

        publisher.subscribe_with(subscriber, :prefix => true)

        publisher.send(:publish, :it_happened)
      end
    end

    # NOTE: these are not realistic use cases, since you would only ever use
    # `scope` when globally subscribing.
    describe ':scope argument' do
      let(:subscriber_1) {double('Listener')  }
      let(:subscriber_2) {double('Listener')  }

      before do
      end

      it 'scopes subscriber to given class' do
        expect(subscriber_1).to receive(:it_happened)
        expect(subscriber_2).not_to receive(:it_happened)
        publisher.subscribe_with(subscriber_1, :scope => publisher.class)
        publisher.subscribe_with(subscriber_2, :scope => Class.new)
        publisher.send(:publish, :it_happened)
      end

      it 'scopes subscriber to given class string' do
        expect(subscriber_1).to receive(:it_happened)
        expect(subscriber_2).not_to receive(:it_happened)
        publisher.subscribe_with(subscriber_1, :scope => publisher.class.to_s)
        publisher.subscribe_with(subscriber_2, :scope => Class.new.to_s)
        publisher.send(:publish, :it_happened)
      end

      it 'includes all subclasses of given class' do
        publisher_super_klass = publisher_class
        publisher_sub_klass = Class.new(publisher_super_klass)

        subscriber = double('Listener')
        expect(subscriber).to receive(:it_happened).once

        publisher = publisher_sub_klass.new

        publisher.subscribe_with(subscriber, :scope => publisher_super_klass)
        publisher.send(:publish, :it_happened)
      end
    end

    it 'returns publisher so methods can be chained' do
      expect(publisher.subscribe_with(subscriber, :on => :so_did_this)).to eq publisher
    end

  end

  describe '.subscribe_with block' do
    let(:insider) { double('insider') }

    it 'subscribes given block to all events' do
      expect(insider).to receive(:it_happened).twice

      publisher.subscribe_with do
        insider.it_happened
      end

      publisher.send(:publish, :something_happened)
      publisher.send(:publish, :and_so_did_this)
    end

    describe ':on argument' do
      it '.add_block_subscriber subscribes block to an event' do
        expect(insider).not_to receive(:it_happened).once

        publisher.subscribe_with(:on => :something_happened) do
          insider.it_happened
        end

        publisher.send(:publish, :something_happened)
        publisher.send(:publish, :and_so_did_this)
      end

      it '.add_block_subscriber subscribes block to all listed events' do
        expect(insider).to receive(:it_happened).twice

        publisher.subscribe_with(
            :on => [:something_happened, :and_so_did_this]) do
          insider.it_happened
        end

        publisher.send(:publish, :something_happened)
        publisher.send(:publish, :and_so_did_this)
        publisher.send(:publish, :but_not_this)
      end
    end

    it 'returns publisher so methods can be chained' do
      expect(publisher.subscribe_with(:on => :this_thing_happened) do
      end).to eq publisher
    end
  end

  describe '.on (alternative block syntax)' do
    let(:insider) { double('insider') }

    it 'subscribes given block to an event' do
      expect(insider).to receive(:it_happened)

      publisher.on(:something_happened) do
        insider.it_happened
      end

      publisher.send(:publish, :something_happened)
    end

    it 'subscribes given block to multiple events' do
      expect(insider).to receive(:it_happened).twice

      publisher.on(:something_happened, :and_so_did_this) do
        insider.it_happened
      end

      publisher.send(:publish, :something_happened)
      publisher.send(:publish, :and_so_did_this)
      publisher.send(:publish, :but_not_this)
    end
  end

  describe '.publish' do
    it 'does not publish events which cannot be responded to' do
      expect(subscriber).not_to receive(:so_did_this)
      allow(subscriber).to receive(:respond_to?).and_return(false)

      publisher.subscribe_with(subscriber, :on => :so_did_this)

      publisher.send(:publish, :so_did_this)
    end

    describe ':event argument' do
      it 'is indifferent to string and symbol' do
        expect(subscriber).to receive(:this_happened).twice

        publisher.subscribe_with(subscriber)

        publisher.send(:publish, 'this_happened')
        publisher.send(:publish, :this_happened)
      end

      it 'is indifferent to dasherized and underscored strings' do
        expect(subscriber).to receive(:this_happened).twice

        publisher.subscribe_with(subscriber)

        publisher.send(:publish, 'this_happened')
        publisher.send(:publish, 'this-happened')
      end
    end
  end

  describe '.subscribers' do
    it 'returns an immutable collection' do
      expect(publisher.subscribers).to be_frozen
      expect { publisher.subscribers << subscriber }.to raise_error(RuntimeError)
    end

    it 'returns local subscribers' do
      publisher.subscribe_with(subscriber)
      expect(publisher.subscribers).to eq [subscriber]
      expect(publisher.subscribers.size).to eq(1)
    end
  end

end