require 'spec_helper'

describe PubSub::GlobalStore do

  # it 'hides actual .globals accessor' do
  #   expect(PubSub::GlobalStore).to_not respond_to(:globals, 'globals=')
  # end

  it 'should be empty by default' do
    expect(PubSub::GlobalStore.keys).to eq([])
  end

  it '.globals persists custom thread local data' do
    something = double('something')
    PubSub::GlobalStore.set(:something, something)
    expect(PubSub::GlobalStore.get(:something)).to be(something)

    with_new_thread do
      expect(PubSub::GlobalStore.keys).to eq([])
      another_something = double('another_something')
      PubSub::GlobalStore.set(:something, another_something)
      expect(PubSub::GlobalStore.get(:something)).to be(another_something)
    end

    expect(PubSub::GlobalStore.get(:something)).to be(something)
  end

end
