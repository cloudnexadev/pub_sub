require 'spec_helper'

describe PubSub::Config do

  describe 'when initialized' do

    it 'should be enabled' do
      expect(subject).to be_enabled
    end

    it 'should have default subscription provider' do
      expect(subject.default_subscription_provider.is_a?(PubSub::SubscriptionProviders::InMemoryProvider))
    end

    it 'should have no custom subscription providers registered' do
      expect(subject.custom_subscription_providers.size).to eq(0)
    end

    it 'should have empty global payload' do
      expect(subject.global_payload).to eq({})
    end

  end

end