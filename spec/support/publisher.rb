# returns an anonymous publisher class
def publisher_class
  Class.new { include PubSub::Publisher }
end