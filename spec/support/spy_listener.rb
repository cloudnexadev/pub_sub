class SpyListener

  def initialize
    @store = []
  end

  def on_event_published(event, publisher, payload = {})
    @store << {:event => event, :publisher => publisher, :payload => payload}
  end

  def logs
    @store
  end

  def clear
    @store.clear
  end

end