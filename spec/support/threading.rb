def with_new_thread
  Thread.new{ yield}.join
end