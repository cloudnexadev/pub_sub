require 'spec_helper'

describe 'Subscription DSL' do

  shared_examples_for 'subscription capable from publisher side' do |subscription_method|
    it 'throws error when no class/instance/block subscribers provided' do
      expect { subject.send(subscription_method) }.to raise_exception
    end

    context 'with block subscriber' do

      it 'allows adding with default options' do
        expect { subject.send(subscription_method){} }.to_not raise_exception
      end

      it 'allows adding with custom options' do
        expect { subject.send(subscription_method, {on: :test}){} }.to_not raise_exception
      end

      it 'allows adding custom subscriber including block with custom options' do
        expect { subject.send(subscription_method, double('subscriber'), on: :test){} }.to_not raise_exception
      end

      it 'allows adding multiple subscribers including block with custom options' do
        expect { subject.send(subscription_method, double('subscriber1'), double('subscriber2'), on: :test){} }.to_not raise_exception
      end

    end

    context 'without block subscriber' do

      it 'allows adding custom subscriber with default options' do
        expect { subject.send(subscription_method, double('subscriber')) }.to_not raise_exception
      end

      it 'allows adding multiple custom subscribers with default options' do
        expect { subject.send(subscription_method, double('subscriber1'), double('subscriber2')) }.to_not raise_exception
      end

      it 'allows adding multiple custom subscribers with custom options' do
        expect { subject.send(subscription_method, double('subscriber1'), double('subscriber2'), on: :test) }.to_not raise_exception
      end

    end
  end

  shared_examples_for 'subscription capable from subscriber side' do

    let(:publisher) { publisher_class.new }

    it 'allows subscription to any publisher' do
      expect { subject.subscribe_to(:all) }.to_not raise_exception
    end

    it 'allows subscription to an instance' do
      expect { subject.subscribe_to(publisher) }.to_not raise_exception
    end

    it 'allows subscription to an instance with custom options' do
      expect { subject.subscribe_to(publisher, on: :test) }.to_not raise_exception
    end

    it 'allows subscription to a class' do
      expect { subject.subscribe_to(publisher_class) }.to_not raise_exception
    end

    it 'allows subscription to a class with custom options' do
      expect { subject.subscribe_to(publisher_class, on: :test) }.to_not raise_exception
    end

  end

  describe 'From Publisher side' do

    describe 'using instance' do
      subject { publisher_class.new }
      it_should_behave_like 'subscription capable from publisher side', :subscribe_with
    end

    describe 'using class' do
      subject { publisher_class }
      it_should_behave_like 'subscription capable from publisher side', :subscribe_with
    end

  end

  describe 'From Subscriber side' do

    describe 'using instance' do
      let(:subscriber_class) { Class.new { include PubSub::Subscriber } }
      subject { subscriber_class.new }
      it_should_behave_like 'subscription capable from subscriber side'
    end

    describe 'using class' do
      subject { Class.new { include PubSub::Subscriber } }
      it_should_behave_like 'subscription capable from subscriber side'
    end

  end

  describe 'Globally using PubSub' do
    subject { PubSub }
    it_should_behave_like 'subscription capable from publisher side', :add_subscription
  end

end