require 'spec_helper'

describe 'Global Payloads' do

  let(:subscriber) { double('subscriber') }
  subject { publisher_class.new }

  it 'can use constant' do
    PubSub.config.global_payload = {:const => 1}
    subject.subscribe_with(subscriber)
    expect(subscriber).to receive('event').with('event', subject, hash_including(:const => 1))
    subject.publish(:event)
  end

  it 'can use Proc' do
    PubSub.config.global_payload = {:proc => Proc.new{|e, p| 1 }}
    subject.subscribe_with(subscriber)
    expect(subscriber).to receive(:event).with('event', subject, hash_including(:proc => 1))
    subject.publish(:event)
  end

  it 'can use any class responding to fetch_data' do
    fetcher = double('fetcher')
    expect(fetcher).to receive(:fetch_data).and_return(:fetch_result)
    PubSub.config.global_payload = {:klass => fetcher}
    subject.subscribe_with(subscriber)
    expect(subscriber).to receive(:event).with('event', subject, hash_including(:klass => :fetch_result))
    subject.publish(:event)
  end

  it 'should not evaluate global payload key if present in payload arg' do
    PubSub.config.global_payload = {:const => 1}
    subject.subscribe_with(subscriber)
    expect(subscriber).to receive('event').with('event', subject, hash_including(:const => 2))
    subject.publish(:event, {:const => 2})
  end

end