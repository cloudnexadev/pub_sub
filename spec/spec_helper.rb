require 'no_pub_sub_spec_helper'
require 'pub_sub'

require 'action_view/test_case'
require 'action_controller/test_case'

Dir[File.join(File.dirname(__FILE__),"support/**/*.rb")].each { |f| require f }

RSpec.configure do |config|
  #config.raise_errors_for_deprecations!
  config.run_all_when_everything_filtered = true
  config.filter_run :focus
  config.order = 'random'

  config.after(:each) { PubSub.reset }
end
